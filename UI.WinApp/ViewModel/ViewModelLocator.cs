/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:UI.WinApp"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Microsoft.Practices.ServiceLocation;
using Services.Contracts;
using System.Windows.Navigation;
using UI.WinApp.Services;
using UI.WinApp.View;

namespace UI.WinApp.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);


            if (ViewModelBase.IsInDesignModeStatic)
            {
                // Create design time view services and models
                // SimpleIoc.Default.Register<IDataService, DesignDataService>();
            }
            else
            {
                // Create run time view services and models
                SimpleIoc.Default.Register<IDialogService, DialogService>();

                var svc = new WindowService();
                configureNavigation(svc);
                SimpleIoc.Default.Register<INavigationService>(() => svc);

                SimpleIoc.Default.Register(ServiceFactory.CreateChannel<Inventory>);
            }

            SimpleIoc.Default.Register<ProductListViewModel>();
            SimpleIoc.Default.Register<ProductFormViewModel>();
        }

        private void configureNavigation(WindowService svc)
        {
            svc.RegisterPage("ProductForm", typeof(ProductForm));
        }

        public ProductListViewModel ProductList
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ProductListViewModel>();
            }
        }

        public ProductFormViewModel ProductForm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ProductFormViewModel>();
            }
        }
        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}