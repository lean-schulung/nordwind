﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Views;
using Services.Contracts;
using Services.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.WinApp.ViewModel
{
    public class ProductListViewModel : ViewModelBase
    {

        #region Bindable Properties
        public string Title { get; set; }
        public List<Category> Categories { get; set; }

        private Category _category;
        public Category CurrentCategory
        {
            get { return _category; }
            set {
                if (_category == value) return;
                _category = value;
                ProductsByCategory = svc.GetProductsByCategory(value.CategoryID);
            }
        }

        private List<ProductInfo> _products;
        public List<ProductInfo> ProductsByCategory
        {
            get { return _products; }
            set {
                _products = value;
                RaisePropertyChanged();
            }
        }

        public ProductInfo CurrentProduct { get; set; }
        #endregion

        #region Commands

        public RelayCommand Create { get; }
        public RelayCommand Edit { get; }
        public RelayCommand Delete { get; }

        #endregion

        // The WCF Client
        private readonly Inventory svc;

        // The WPF patterns
        private readonly IDialogService dialogSvc;
        private readonly INavigationService navigationSvc;

        public ProductListViewModel(
            Inventory inventory, 
            INavigationService navigationService,
            IDialogService dialogService)
        {
            svc = inventory;
            dialogSvc = dialogService;
            navigationSvc = navigationService;

            // Setting Props
            Title = "Artikel-Auswahl";
            Categories = svc.GetAllCategories();

            // Wiring Commands
            Create = new RelayCommand(ExecuteCreate);
            Edit = new RelayCommand(ExecuteEdit, CanExecuteEdit);
            Delete = new RelayCommand(ExecuteDelete, CanExecuteDelete);
        }

        private void ExecuteCreate()
        {
            navigationSvc.NavigateTo("ProductForm");
        }

        private void ExecuteEdit()
        {
            navigationSvc.NavigateTo("ProductForm", CurrentProduct.ProductId);
        }

        private void ExecuteDelete()
        {
            var confirmed = 
                dialogSvc.ShowMessage("Delete the Product?", "Warning", "", "", null).Result;    
            
            if(confirmed)
               svc.DeleteProduct(CurrentProduct.ProductId);
        }

        private bool CanExecuteEdit()
        {
            return CurrentProduct != null;
        }
        private bool CanExecuteDelete()
        {
            return CurrentProduct != null;
        }

    }
}
