﻿using GalaSoft.MvvmLight;
using Services.Contracts;
using Services.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.WinApp.ViewModel
{
    public class ProductFormViewModel : ViewModelBase, INavigatable
    {
        private Product _current;

        public Product Current
        {
            get { return _current; }
            set { _current = value; RaisePropertyChanged(); }
        }

        private readonly Inventory svc;
        public ProductFormViewModel(Inventory inventory)
        {
            svc = inventory;
        }
        public void NavigatedTo(object param)
        {
            Current = svc.GetProductById((int)param);
        }
    }
}
