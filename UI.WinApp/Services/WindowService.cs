﻿using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UI.WinApp.ViewModel;

namespace UI.WinApp.Services
{
    public class WindowService : INavigationService
    {
        private Dictionary<string, Type> pages = new Dictionary<string, Type>();
        public void RegisterPage(string pakeKey, Type window)
        {
            pages.Add(pakeKey, window);
        }

        public string CurrentPageKey => throw new NotImplementedException();

        public void GoBack()
        {
            throw new NotImplementedException();
        }

        public void NavigateTo(string pageKey)
        {
            var type = pages[pageKey];

            var window = (Activator.CreateInstance(type) as Window);
            window.Show();
        }

        public void NavigateTo(string pageKey, object parameter)
        {
            var type = pages[pageKey];

            var window = (Activator.CreateInstance(type) as Window);
            (window.DataContext as INavigatable).NavigatedTo(parameter);

            window.Show();
        }
    }
}
