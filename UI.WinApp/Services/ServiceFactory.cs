﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace UI.WinApp.Services
{
    public static class ServiceFactory
    {
        public static T CreateChannel<T>()
        {
            var cf = new ChannelFactory<T>(typeof(T).Name);
            return cf.CreateChannel();
        }
    }
}
