﻿using Datalayer;
using Services.Contracts;
using Services.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Services.Implementations
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class InventoryImpl : Inventory
    {
        NorthwindCtx ctx = new NorthwindCtx();

        public List<Category> GetAllCategories()
        {
            var categories = ctx.Categories
                .Select(c => new Category()
                {
                    CategoryID = c.CategoryID,
                    CategoryName = c.CategoryName,
                    Description = c.Description
                });
            return categories.ToList();
        }

        public List<ProductInfo> GetProductsByCategory(int categoryId)
        {
            return ctx.Products
                .Where(p => p.CategoryID == categoryId)
                .Select(p => new ProductInfo()
                {
                    ProductId = p.ProductID,
                    ProductName = p.ProductName,
                    QuantityPerUnit = p.QuantityPerUnit,
                    UnitPrice = p.UnitPrice
                }).ToList();
        }

        public Product GetProductById(int productId)
        {
            var p = ctx.Products.Find(productId);
            return new Product() {
                ProductID = p.ProductID,
                CategoryID = p.CategoryID ?? 0,
                ProductName = p.ProductName,
                QuantityPerUnit = p.QuantityPerUnit,
                UnitPrice = p.UnitPrice ?? 0.0m,
                Discontinued = p.Discontinued
            };
        }

        public Product CreateProduct(Product p)
        {
            var product = new Datalayer.Model.Product
            {
                CategoryID = p.CategoryID,
                ProductName = p.ProductName,
                QuantityPerUnit = p.QuantityPerUnit,
                UnitPrice = p.UnitPrice,
                Discontinued = p.Discontinued,
                UnitsInStock = 0,
                ReorderLevel = 0
            };

            ctx.Products.Add(product);
            ctx.SaveChanges();

            p.ProductID = product.ProductID;
            return p;
        }

        public void UpdateProduct(Product p)
        {
            // Todo: better security and concurrency

            var product = ctx.Products.Find(p.ProductID);
            ctx.Entry(product).CurrentValues.SetValues(p);
            ctx.SaveChanges();
        }

        public void DeleteProduct(int productId)
        {
            // Todo: better security and concurrency

            var product = ctx.Products.Find(productId);

            ctx.Products.Remove(product);
            ctx.SaveChanges();
        }
    }
}
