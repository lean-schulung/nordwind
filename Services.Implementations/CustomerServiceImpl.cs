﻿using Datalayer;
using Datalayer.Model;
using Services.Contracts;
using Services.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementations
{
    public class CustomerServiceImpl : CustomerService
    {
        NorthwindCtx ctx = new NorthwindCtx();

        public void Create(CustomerDTO c)
        {
            var customer = new Customer() {
                CustomerID = c.CustomerID,
                CompanyName = c.CompanyName,
                ContactName = c.ContactName,
                ContactTitle = c.ContactTitle,
                Address = c.Address,
                City = c.City,
                Region = c.Region,
                Country = c.Country,
                PostalCode = c.PostalCode,
                Fax = c.Fax,
                Phone = c.Phone
            };
            ctx.Customers.Add(customer);
            ctx.SaveChanges();
        }

        public void Delete(string customerID)
        {
            var customer = ctx.Customers.Find(customerID);
            ctx.Customers.Remove(customer);
            ctx.SaveChanges();
        }

        public List<CustomerDTO> GetAll()
        {
            var customers = ctx.Customers.Select(c => new CustomerDTO() {
                CustomerID = c.CustomerID,
                CompanyName = c.CompanyName,
                ContactName = c.ContactName,
                ContactTitle = c.ContactTitle,
                Address = c.Address,
                City = c.City,
                Region = c.Region,
                Country = c.Country,
                PostalCode = c.PostalCode,
                Fax = c.Fax,
                Phone = c.Phone
            });

            return customers.ToList();
        }

        public void Update(CustomerDTO c)
        {
            var customer = new Customer()
            {
                CustomerID = c.CustomerID,
                CompanyName = c.CompanyName,
                ContactName = c.ContactName,
                ContactTitle = c.ContactTitle,
                Address = c.Address,
                City = c.City,
                Region = c.Region,
                Country = c.Country,
                PostalCode = c.PostalCode,
                Fax = c.Fax,
                Phone = c.Phone
            };
            ctx.Entry(customer).State = System.Data.Entity.EntityState.Modified;
            ctx.SaveChanges();
        }
    }
}
