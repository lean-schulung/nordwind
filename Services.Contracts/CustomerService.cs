﻿using Services.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    [ServiceContract(Namespace = "http://lean-stack.de/northwind")]
    public interface CustomerService
    {
        [OperationContract]
        List<CustomerDTO> GetAll();

        [OperationContract]
        void Create(CustomerDTO customer);

        [OperationContract]
        void Update(CustomerDTO customer);

        [OperationContract]
        void Delete(string customerID);
    }
}
