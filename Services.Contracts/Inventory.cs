﻿using Services.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    [ServiceContract(Namespace = "http://lean-stack.de/northwind")]
    public interface Inventory
    {
        [OperationContract]
        List<Category> GetAllCategories();

        [OperationContract]
        List<ProductInfo> GetProductsByCategory(int categoryId);

        [OperationContract]
        Product GetProductById(int productId);

        [OperationContract]
        Product CreateProduct(Product p);

        [OperationContract]
        void UpdateProduct(Product p);

        [OperationContract]
        void DeleteProduct(int productId);
    }
}
