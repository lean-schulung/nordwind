﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.DataContracts
{
    public class ProductInfo
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string QuantityPerUnit { get; set; }
        public decimal? UnitPrice { get; set; }
    }
}
