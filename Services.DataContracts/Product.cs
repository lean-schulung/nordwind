namespace Services.DataContracts
{
    using System.ComponentModel.DataAnnotations;

    public partial class Product
    {
        public int ProductID { get; set; }

        public int CategoryID { get; set; }

        [Required]
        [StringLength(40)]
        public string ProductName { get; set; }

        [StringLength(20)]
        public string QuantityPerUnit { get; set; }

        public decimal UnitPrice { get; set; }

        public bool Discontinued { get; set; }

    }
}
